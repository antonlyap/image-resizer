use actix_web::{middleware::Logger, App, HttpServer, web::Data};
use libvips::VipsApp;

mod routes;
use routes::test::test;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
  std::env::set_var("RUST_LOG", "actix_web=info");
  env_logger::init();

  let app = VipsApp::new("App", true).expect("Failed to create app");
  app.concurrency_set(6);

  let data = Data::new(app);

  HttpServer::new(move || {
    let logger = Logger::default();
    App::new().app_data(data.clone()).wrap(logger).service(test)
  })
  .bind("0.0.0.0:8080")?
  .run()
  .await
}
