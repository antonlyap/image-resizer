use actix_web::{get, web, HttpResponse};
use serde::Deserialize;

use libvips::{VipsImage, ops::{self, JpegsaveBufferOptions}, VipsApp};

#[derive(Debug, Deserialize)]
pub enum Mode {
  Cover,
  Contain,
  Stretch,
}

#[derive(Debug, Deserialize)]
pub struct Querystring {
  name: String,
  width: Option<usize>,
  height: Option<usize>,
  mode: Mode,
}

#[get("/")]
pub async fn test(query: web::Query<Querystring>, app: web::Data<VipsApp>) -> HttpResponse {
  let image_result = VipsImage::new_from_file(
    std::env::current_dir().unwrap().join("images").join(query.name.clone()).to_str().unwrap(),
  );

  if image_result.is_err() {
    println!("{}", image_result.unwrap_err());
    return HttpResponse::NotFound().content_type("text/plain").body("Failed to open file")
  }

  let image = image_result.unwrap();

  let mut scale_x: f64;
  let mut scale_y: f64;
  if query.width.is_some() && query.height.is_some() {
    scale_x = (query.width.unwrap() as f64) / (image.get_width() as f64);
    scale_y = (query.height.unwrap() as f64) / (image.get_height() as f64);
    scale_x = match query.mode {
      Mode::Contain => scale_x.min(scale_y),
      Mode::Cover => scale_x.max(scale_y),
      Mode::Stretch => scale_x,
    };
    scale_y = match query.mode {
      Mode::Contain => scale_x.min(scale_y),
      Mode::Cover => scale_x.max(scale_y),
      Mode::Stretch => scale_y,
    };
  } else if query.width.is_some() {
    scale_x = (query.width.unwrap() as f64) / (image.get_width() as f64);
    scale_y = scale_x;
  } else if query.height.is_some() {
    scale_y = (query.height.unwrap() as f64) / (image.get_height() as f64);
    scale_x = scale_y;
  } else {
    return HttpResponse::BadRequest().content_type("text/plain").body("Either or both width and height must be provided");
  }
  
  let resized_result = ops::resize_with_opts(&image, scale_x, &ops::ResizeOptions {
    vscale: scale_y,
    kernel: ops::Kernel::Lanczos3,
  });

  if resized_result.is_err() {
    println!("{}", resized_result.unwrap_err());
    return HttpResponse::InternalServerError().content_type("text/plain").body("Resize error")
  }

  let resized = resized_result.unwrap();

  let buffer_result = ops::jpegsave_buffer_with_opts(&resized, &ops::JpegsaveBufferOptions {

    q: 70,
    ..JpegsaveBufferOptions::default()
  });

  if buffer_result.is_err() {
    println!("{}", app.error_buffer().unwrap());
    return HttpResponse::InternalServerError().content_type("text/plain").body("Output error")
  }
  
  HttpResponse::Ok().body(buffer_result.unwrap())
}