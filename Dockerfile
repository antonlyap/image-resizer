####################################################################################################
FROM rust:1.59.0-alpine3.15 AS build

RUN apk update && apk add vips vips-dev libc-dev

WORKDIR /opt/app

COPY ./ .

RUN cargo build --release

####################################################################################################
## Final image
####################################################################################################
FROM alpine:3.15

RUN apk update && apk add vips vips-dev libc-dev

WORKDIR /opt/app

# Copy our build
COPY --from=build /opt/app/target/release/ ./

CMD ["/opt/app/image-resizer"]